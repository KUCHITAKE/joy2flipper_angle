﻿// -*- C++ -*-
// <rtc-template block="description">
/*!
 * @file  joy2flipper_angle.cpp
 * @brief convert joy to flipper angle
 *
 */
// </rtc-template>

#include "joy2flipper_angle.h"

// Module specification
// <rtc-template block="module_spec">
#if RTM_MAJOR_VERSION >= 2
static const char *const joy2flipper_angle_spec[] =
#else
static const char *joy2flipper_angle_spec[] =
#endif
    {"implementation_id",
     "joy2flipper_angle",
     "type_name",
     "joy2flipper_angle",
     "description",
     "convert joy to flipper angle",
     "version",
     "1.0.0",
     "vendor",
     "NUT Kimuralab Keitaro Takeuchi",
     "category",
     "Category",
     "activity_type",
     "PERIODIC",
     "kind",
     "DataFlowComponent",
     "max_instance",
     "1",
     "language",
     "C++",
     "lang_type",
     "compile",
     ""};
// </rtc-template>

/*!
 * @brief constructor
 * @param manager Maneger Object
 */
joy2flipper_angle::joy2flipper_angle(RTC::Manager *manager)
    // <rtc-template block="initializer">
    : RTC::DataFlowComponentBase(manager),
      m_joyaxisIn("joyaxis", m_joyaxis),
      m_joybuttonIn("joybutton", m_joybutton),
      m_flipper_angleOut("flipper_angle", m_flipper_angle)
// </rtc-template>
{}

/*!
 * @brief destructor
 */
joy2flipper_angle::~joy2flipper_angle() {}

RTC::ReturnCode_t joy2flipper_angle::onInitialize() {
  // Registration: InPort/OutPort/Service
  // <rtc-template block="registration">
  // Set InPort buffers
  addInPort("joyaxis", m_joyaxisIn);
  addInPort("joybutton", m_joybuttonIn);

  // Set OutPort buffer
  addOutPort("flipper_angle", m_flipper_angleOut);

  // Set service provider to Ports

  // Set service consumers to Ports

  // Set CORBA Service Ports

  // </rtc-template>

  // <rtc-template block="bind_config">
  // </rtc-template>

  return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t joy2flipper_angle::onFinalize()
{
  return RTC::RTC_OK;
}
*/

RTC::ReturnCode_t joy2flipper_angle::onStartup(RTC::UniqueId /*ec_id*/) {
  return RTC::RTC_OK;
}

RTC::ReturnCode_t joy2flipper_angle::onShutdown(RTC::UniqueId /*ec_id*/) {
  return RTC::RTC_OK;
}

RTC::ReturnCode_t joy2flipper_angle::onActivated(RTC::UniqueId /*ec_id*/) {
  return RTC::RTC_OK;
}

RTC::ReturnCode_t joy2flipper_angle::onDeactivated(RTC::UniqueId /*ec_id*/) {
  return RTC::RTC_OK;
}

RTC::ReturnCode_t joy2flipper_angle::onExecute(RTC::UniqueId /*ec_id*/) {
  if (m_joyaxisIn.isNew()) {
    m_joyaxisIn.read();
  }

  if(m_joybuttonIn.isNew()) {
    m_joybuttonIn.read();
  }

  // if (m_joyaxis.data[])

  return RTC::RTC_OK;
}

// RTC::ReturnCode_t joy2flipper_angle::onAborting(RTC::UniqueId /*ec_id*/)
//{
//   return RTC::RTC_OK;
// }

// RTC::ReturnCode_t joy2flipper_angle::onError(RTC::UniqueId /*ec_id*/)
//{
//   return RTC::RTC_OK;
// }

// RTC::ReturnCode_t joy2flipper_angle::onReset(RTC::UniqueId /*ec_id*/)
//{
//   return RTC::RTC_OK;
// }

// RTC::ReturnCode_t joy2flipper_angle::onStateUpdate(RTC::UniqueId /*ec_id*/)
//{
//   return RTC::RTC_OK;
// }

// RTC::ReturnCode_t joy2flipper_angle::onRateChanged(RTC::UniqueId /*ec_id*/)
//{
//   return RTC::RTC_OK;
// }

extern "C" {

void joy2flipper_angleInit(RTC::Manager *manager) {
  coil::Properties profile(joy2flipper_angle_spec);
  manager->registerFactory(profile, RTC::Create<joy2flipper_angle>,
                           RTC::Delete<joy2flipper_angle>);
}
}
