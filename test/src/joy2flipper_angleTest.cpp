﻿// -*- C++ -*-
// <rtc-template block="description">
/*!
 * @file  joy2flipper_angleTest.cpp
 * @brief convert joy to flipper angle (test code)
 *
 */
// </rtc-template>

#include "joy2flipper_angleTest.h"

// Module specification
// <rtc-template block="module_spec">
#if RTM_MAJOR_VERSION >= 2
static const char* const joy2flipper_angle_spec[] =
#else
static const char* joy2flipper_angle_spec[] =
#endif
  {
    "implementation_id", "joy2flipper_angleTest",
    "type_name",         "joy2flipper_angleTest",
    "description",       "convert joy to flipper angle",
    "version",           "1.0.0",
    "vendor",            "NUT Kimuralab Keitaro Takeuchi",
    "category",          "Category",
    "activity_type",     "PERIODIC",
    "kind",              "DataFlowComponent",
    "max_instance",      "1",
    "language",          "C++",
    "lang_type",         "compile",
    ""
  };
// </rtc-template>

/*!
 * @brief constructor
 * @param manager Maneger Object
 */
joy2flipper_angleTest::joy2flipper_angleTest(RTC::Manager* manager)
    // <rtc-template block="initializer">
  : RTC::DataFlowComponentBase(manager),
    m_joyaxisOut("joyaxis", m_joyaxis),
    m_joybuttonOut("joybutton", m_joybutton),
    m_flipper_angleIn("flipper_angle", m_flipper_angle)

    // </rtc-template>
{
}

/*!
 * @brief destructor
 */
joy2flipper_angleTest::~joy2flipper_angleTest()
{
}



RTC::ReturnCode_t joy2flipper_angleTest::onInitialize()
{
  // Registration: InPort/OutPort/Service
  // <rtc-template block="registration">
  // Set InPort buffers
  addInPort("flipper_angle", m_flipper_angleIn);
  
  // Set OutPort buffer
  addOutPort("joyaxis", m_joyaxisOut);
  addOutPort("joybutton", m_joybuttonOut);
  
  // Set service provider to Ports
  
  // Set service consumers to Ports
  
  // Set CORBA Service Ports
  
  // </rtc-template>

  // <rtc-template block="bind_config">
  // </rtc-template>
  
  return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t joy2flipper_angleTest::onFinalize()
{
  return RTC::RTC_OK;
}
*/


RTC::ReturnCode_t joy2flipper_angleTest::onStartup(RTC::UniqueId /*ec_id*/)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t joy2flipper_angleTest::onShutdown(RTC::UniqueId /*ec_id*/)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t joy2flipper_angleTest::onActivated(RTC::UniqueId /*ec_id*/)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t joy2flipper_angleTest::onDeactivated(RTC::UniqueId /*ec_id*/)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t joy2flipper_angleTest::onExecute(RTC::UniqueId /*ec_id*/)
{
  return RTC::RTC_OK;
}


//RTC::ReturnCode_t joy2flipper_angleTest::onAborting(RTC::UniqueId /*ec_id*/)
//{
//  return RTC::RTC_OK;
//}


//RTC::ReturnCode_t joy2flipper_angleTest::onError(RTC::UniqueId /*ec_id*/)
//{
//  return RTC::RTC_OK;
//}


//RTC::ReturnCode_t joy2flipper_angleTest::onReset(RTC::UniqueId /*ec_id*/)
//{
//  return RTC::RTC_OK;
//}


//RTC::ReturnCode_t joy2flipper_angleTest::onStateUpdate(RTC::UniqueId /*ec_id*/)
//{
//  return RTC::RTC_OK;
//}


//RTC::ReturnCode_t joy2flipper_angleTest::onRateChanged(RTC::UniqueId /*ec_id*/)
//{
//  return RTC::RTC_OK;
//}


bool joy2flipper_angleTest::runTest()
{
    return true;
}


extern "C"
{
 
  void joy2flipper_angleTestInit(RTC::Manager* manager)
  {
    coil::Properties profile(joy2flipper_angle_spec);
    manager->registerFactory(profile,
                             RTC::Create<joy2flipper_angleTest>,
                             RTC::Delete<joy2flipper_angleTest>);
  }
  
}
